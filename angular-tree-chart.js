(function () {
    'use strict';

    angular.module('angularTreeChart', [])
        .directive('treeChart', [function () {
            return {
                scope: {
                    options: '=',
                    data: '=',
                    api: '='
                },
                restrict: 'AE',
                link: function (scope, element) {

                    var link, node, width, height, formatNumber, color, svg, sankey, path,
                        nodeWidth = scope.options.nodeWidth || 20,
                        margin = {top: 20, right: 100, left: 20, bottom: 20},
                        textPadding = {bottom: 5};

                    var formatValue = scope.options.formatValue || function (d) {
                            return d.value;
                        };

                    var rootNameFormat = scope.options.rootNameFormat;


                    element.css({width: '100%', height: '100%', display: 'block'});
                    element.addClass('tree-chart');

                    var elementHeightListener = scope.$watch(function () {
                        return element.height()
                    }, function (elementHeight) {
                        if (elementHeight > 0) {
                            initSankey();
                            elementHeightListener();
                        }
                    });

                    function initSankey() {

                        width = element.width() - margin.right - margin.left;
                        height = element.height() - margin.top - margin.bottom;

                        formatNumber = d3.format(",.0f");

                        color = d3.scale.category20();

                        svg = d3.select(element[0]).append("svg")
                            .attr("width", width + margin.right + margin.left)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                        sankey = d3.sankey()
                            .nodeWidth(nodeWidth)
                            .nodePadding(20)
                            .size([width, height]);

                        path = sankey.link();

                        scope.$watch('data', function (data) {

                            if (data) {
                                svg.attr('opacity', 1)
                                    .transition()
                                    .attr('opacity', 0)
                                    .each('end', function () {
                                        if (link) link.remove();
                                        if (node) node.remove();

                                        updateWithData(data);

                                        svg.attr('opacity', 0)
                                            .transition()
                                            .attr('opacity', 1);
                                    });
                            }

                        });
                    }

                    function updateSize() {

                        if (node) {
                            width = element.width() - margin.right - margin.left;
                            height = element.height() - margin.top - margin.bottom;

                            sankey
                                .size([width, height]).layout(0);
                            sankey.relayout();

                            d3.select(element[0])
                                .select('svg')
                                .attr('height', height + margin.top + margin.left)
                                .attr('width', width + margin.right + margin.bottom);

                            node.transition()
                                .attr('transform', function (d) {
                                    if (d.root) d.y = 0;
                                    return "translate(" + d.x + "," + d.y + ")";
                                });
                            link.transition()
                                .attr('d', path)
                                .style("stroke-width", function (d) {
                                    return Math.max(1, d.dy);
                                })
                                .attr('stroke-opacity', function (d) {
                                    return d.linkOpacity;
                                });
                            node.select('rect')
                                .transition()
                                .attr('width', sankey.nodeWidth())
                                .attr('height', function (d) {
                                    return Math.max(1, d.dy);
                                })
                                .attr('y', function (d) {
                                    if (d.dy < 1) {
                                        return -0.5;
                                    }
                                });
                            // add label text
                            node.select(".name")
                                .transition()
                                .attr("y", labelY);

                            // add value text
                            node.select('.value')
                                .transition()
                                .attr("y", function (d) {
                                    if (d.root) return labelY(d) + 20;
                                    return labelY(d);
                                });

                            svg.select('.decorator')
                                .transition()
                                .attr('y', decoratorY)
                                .attr("height", decoratorHeight)
                                .style('stroke-dasharray', decoratorBorder)
                        }
                    }


                    function updateWithData(data) {

                        sankey
                            .nodes(scope.data.nodes)
                            .links(scope.data.links)
                            .layout(0);

                        link = svg.append("g").selectAll(".link")
                            .data(scope.data.links)
                            .enter().append("path")
                            .attr("class", "link")
                            .attr("d", path)
                            .style("stroke-width", function (d) {
                                return Math.max(1, d.dy);
                            })
                            .style('stroke', function (d) {
                                return d.linkColor;
                            });

                        node = svg.append("g").selectAll(".node")
                            .data(scope.data.nodes)
                            .enter().append("g")
                            .attr("class", "node")
                            .attr("transform", function (d) {
                                return "translate(" + d.x + "," + d.y + ")";
                            })
                            .attr('class', function (d) {
                                if (d.root) return 'root node';
                                return 'node';
                            })
                            .sort(function (a, b) {
                                return (a.value - b.value);
                            });

                        node.append("rect")
                            .attr("height", function (d) {
                                return Math.max(1, d.dy);
                            })
                            .attr("width", sankey.nodeWidth())
                            .attr('y', function (d) {
                                if (d.dy < 1) {
                                    return -0.5;
                                }
                            })
                            .style("fill", function (d) {
                                return '#FBCD06';
                            })
                            .append("title");

                        // add label text
                        node.append("text")
                            .attr("x", function (d) {
                                if (d.root) return 0;
                                return -5;
                            })
                            .attr("y", labelY)
                            .attr('text-anchor', function (d) {
                                if (d.root) return 'start';
                                return 'end';
                            })
                            .attr("ver", null)
                            .text(function (d) {
                                if (d.root) {
                                    if (rootNameFormat) return rootNameFormat(d);
                                    return d.name;
                                }
                                return d.name;
                            })
                            .classed('name', true);

                        // add value text
                        node.append('text')
                            .attr("x", function (d) {

                                if(d.root) {
                                    return 0;
                                }

                                return sankey.nodeWidth() + 5;
                            })
                            .attr("y", function (d) {
                                if (d.root) return labelY(d) + 20;
                                return labelY(d);
                            })
                            .attr("ver", null)
                            .text(formatValue)
                            .classed('value', true);

                        sankey.relayout();
                        link.attr('d', path);
                    }

                    function labelY(d) {
                        if (d.root) {
                            return d.dy + 20;
                        } else {
                            return d.dy - textPadding.bottom;
                        }
                    }

                    function decoratorHeight(d) {
                        return height - d.dy - 10;
                    }

                    function decoratorY(d) {
                        return d.dy;
                    }

                    function decoratorBorder(d) {
                        var h = height - d.dy - 10,
                            w = sankey.nodeWidth(),
                            stroke = '0, ' + (w + h) + ', ' + w + ', ' + h;
                        return stroke;
                    }

                    window.addEventListener('resize', function () {
                        updateSize();
                    });

                    scope.api = {
                        update: function () {
                            updateSize();
                        }
                    };
                }
            }
        }])
})();